package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author kaiyi
 * @email corwienwong@gmail.com
 * @date 2020-08-11 01:28:15
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {

  MemberLevelEntity getDefaultLevel();
}
