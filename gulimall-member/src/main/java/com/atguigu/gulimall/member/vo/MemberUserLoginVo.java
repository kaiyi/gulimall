package com.atguigu.gulimall.member.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-09-08 16:36
 */
@Data
public class MemberUserLoginVo {
  private String loginacct;
  private String password;

}
