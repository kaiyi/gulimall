package com.atguigu.gulimall.member.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-09-08 16:35
 */
@Data
public class MemberUserRegisterVo {

  private String userName;

  private String password;

  private String phone;

}