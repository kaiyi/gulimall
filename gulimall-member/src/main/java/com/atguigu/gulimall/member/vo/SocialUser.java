package com.atguigu.gulimall.member.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-09-08 16:41
 */
@Data
public class SocialUser {

  private String access_token;

  private String remind_in;

  private long expires_in;

  private String uid;

  private String isRealName;

}