package com.atguigu.gulimall.member.exception;

/**
 * @author: kaiyi
 * @create: 2020-09-08 16:47
 */
public class PhoneException extends RuntimeException{

  public PhoneException(){
    super("存在相同的手机号");
  }

}
