package com.atguigu.gulimall.member.exception;

/**
 * @author: kaiyi
 * @create: 2020-09-08 16:48
 */
public class UsernameException extends RuntimeException {

  public UsernameException(){
    super("存在相同的用户名");
  }


}
