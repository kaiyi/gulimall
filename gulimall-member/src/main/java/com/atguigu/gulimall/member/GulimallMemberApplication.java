package com.atguigu.gulimall.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


@EnableFeignClients(basePackages = "com.atguigu.gulimall.member.feign") //在服务启动时会自动扫描Feign包下的路径
@EnableDiscoveryClient // 添加注册发现功能
@MapperScan("com.atguigu.gulimall.member.dao")
@SpringBootApplication
public class GulimallMemberApplication {

  public static void main(String[] args) {
    SpringApplication.run(GulimallMemberApplication.class, args);
  }

}
