package com.atguigu.common.constant;

/**
 * @author: kaiyi
 * @create: 2020-09-07 23:48
 */
public class AuthServerConstant {

  public static final String SMS_CODE_CACHE_PREFIX = "sms:code";

  public static final String LOGIN_USER = "loginUser";

}
