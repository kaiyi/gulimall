package com.atguigu.common.constant;

/**
 * 购物车常量
 *
 * @author: kaiyi
 * @create: 2020-09-12 11:30
 */
public class CartConstant {

  public final static String TEMP_USER_COOKIE_NAME = "user-key";

  public final static int TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30;

  public final static String CART_PREFIX = "gulimall:cart:";

}
