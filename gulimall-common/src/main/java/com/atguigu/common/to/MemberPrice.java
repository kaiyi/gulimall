package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: kaiyi
 * @create: 2020-08-26 14:26
 */
@Data
public class MemberPrice {

  private Long id;
  private String name;
  private BigDecimal price;

}