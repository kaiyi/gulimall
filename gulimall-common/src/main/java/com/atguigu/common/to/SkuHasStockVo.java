package com.atguigu.common.to;

import lombok.Data;

/**
 * to：表示数据传输层
 *
 * @author: kaiyi
 * @create: 2020-08-27 11:57
 */
@Data
public class SkuHasStockVo {
  private Long skuId;
  private Boolean hasStock;

}
