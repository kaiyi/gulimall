package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: kaiyi
 * @create: 2020-08-26 14:23
 */
@Data
public class SpuBoundTo {

  private Long spuId;
  private BigDecimal buyBounds;
  private BigDecimal growBounds;
}