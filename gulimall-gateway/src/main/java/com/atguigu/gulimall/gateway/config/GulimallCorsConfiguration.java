package com.atguigu.gulimall.gateway.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.reactive.CorsWebFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;

/**
 * @author: kaiyi
 * @create: 2020-08-17 11:47
 */

@Configuration
public class GulimallCorsConfiguration {

  @Bean
  public CorsWebFilter corsWebFilter(){
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    CorsConfiguration corsConfiguration = new CorsConfiguration();

    // 1、配置跨域
    corsConfiguration.addAllowedHeader("*");
    corsConfiguration.addAllowedMethod("*");
    corsConfiguration.addAllowedOrigin("*");
    corsConfiguration.setAllowCredentials(true);  // 允许cookie


    source.registerCorsConfiguration("/**", corsConfiguration);
    // 只需要将跨域配置信息放入到该Filter就起作用了
    return new CorsWebFilter(source);
  }


}
