package com.atguigu.gulimall.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * 1、启动注册自动发现
 * 2、排除数据源自动配置（因为网关服务不需要用到数据库方便的功能）
 */
@EnableDiscoveryClient
// 排除数据源自动配置
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
public class GulimallGatewayApplication {

  public static void main(String[] args) {
    SpringApplication.run(GulimallGatewayApplication.class, args);
  }

}
