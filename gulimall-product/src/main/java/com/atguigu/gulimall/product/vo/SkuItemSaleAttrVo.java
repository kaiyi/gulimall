package com.atguigu.gulimall.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author: kaiyi
 * @create: 2020-09-05 09:54
 */
@Data
@ToString
public class SkuItemSaleAttrVo {

  private Long attrId;

  private String attrName;

  private List<AttrValueWithSkuIdVo> attrValues;

}
