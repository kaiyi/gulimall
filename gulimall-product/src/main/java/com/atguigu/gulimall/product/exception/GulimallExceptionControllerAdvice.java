package com.atguigu.gulimall.product.exception;

import com.atguigu.common.exception.BizCodeEnum;
import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

/**
 * 集中处理所有异常
 *
 * @author: kaiyi
 * @create: 2020-08-21 00:56
 */

@Slf4j
//@ResponseBody
//@ControllerAdvice(basePackages = "com.atguigu.gulimall.product.controller")
// 处理product.controller包下边的异常
@RestControllerAdvice(basePackages = "com.atguigu.gulimall.product.controller")
public class GulimallExceptionControllerAdvice {

  @ExceptionHandler(value = MethodArgumentNotValidException.class)
  public R handleValidException(MethodArgumentNotValidException e){
    log.error("数据校验出现问题{}，异常类型：{}", e.getMessage(), e.getClass());
    BindingResult bindingResult = e.getBindingResult();

    Map<String, String> errorMap = new HashMap<>();
    bindingResult.getFieldErrors().forEach((fieldError)->{
      errorMap.put(fieldError.getField(),fieldError.getDefaultMessage());
    });

    return R.error(BizCodeEnum.VALID_EXCEPTION.getCode(),
        BizCodeEnum.VALID_EXCEPTION.getMsg()).put("data", errorMap);

  }

  /**
   * 通用的异常处理方法
   * @description 如果找不到具体的异常处理方法，则由该方法处理
   *
   * @param throwable
   * @return
   */
  @ExceptionHandler(value = Throwable.class)
  public R handleException(Throwable throwable){
    log.error("错误:", throwable);
    return R.error(BizCodeEnum.UNKNOW_EXCEPTION.getCode(), BizCodeEnum.UNKNOW_EXCEPTION.getMsg());

  }


}
