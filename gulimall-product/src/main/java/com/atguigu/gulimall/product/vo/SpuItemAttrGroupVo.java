package com.atguigu.gulimall.product.vo;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author: kaiyi
 * @create: 2020-09-05 09:56
 */
@Data
@ToString
public class SpuItemAttrGroupVo {
  private String groupName;

  private List<Attr> attrs;

}
