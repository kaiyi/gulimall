package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-09-05 10:00
 */
@Data
public class AttrValueWithSkuIdVo {
  private String attrValue;
  private String skuIds;
}
