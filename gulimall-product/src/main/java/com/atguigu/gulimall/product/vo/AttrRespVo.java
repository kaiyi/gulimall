package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-08-23 14:39
 */
@Data
public class AttrRespVo extends AttrVo {

  private String catelogName;
  private String groupName;

  private Long[] catelogPath;

}
