package com.atguigu.gulimall.product.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author: kaiyi
 * @create: 2020-09-05 18:59
 */
// @EnableConfigurationProperties(ThreadPoolConfigProperties.class)
// 我们的配置属性已经加入到容器中了(component)，所以，不再需要开启上边的属性配置了
@Configuration
public class MyThreadConfig {

  @Bean
  public ThreadPoolExecutor threadPoolExecutor(ThreadPoolConfigProperties pool) {

    /*
    这里是写死的参数，需要从配置文件里边获取相关参数
    return new ThreadPoolExecutor(
        20,
        200,
        10,
        pool.getKeepAliveTime(),
        TimeUnit.SECONDS,
        new LinkedBlockingQueue<>(100000),
        Executors.defaultThreadFactory(),
        new ThreadPoolExecutor.AbortPolicy()
    );
     */

    return new ThreadPoolExecutor(
        pool.getCoreSize(),
        pool.getMaxSize(),
        pool.getKeepAliveTime(),
        TimeUnit.SECONDS,
        new LinkedBlockingQueue<>(100000),
        Executors.defaultThreadFactory(),
        new ThreadPoolExecutor.AbortPolicy()
    );

  }

}
