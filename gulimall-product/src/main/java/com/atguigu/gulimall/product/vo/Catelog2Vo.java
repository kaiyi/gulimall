package com.atguigu.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 二级分类VO
 *
 * @author: kaiyi
 * @create: 2020-08-29 17:03
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Catelog2Vo {
  private String catalog1Id; // 1级父分类id
  private List<Catelog3Vo> catalog3List;  // 三级子分类
  private String id;
  private String name;

  /**
   * 三级分类Vo
   *
   *  {
   *         "catalog2Id": "17",
   *         "id": "100",
   *         "name": "燃气灶"
   *     },
   */
  @NoArgsConstructor
  @AllArgsConstructor
  @Data
  public static class Catelog3Vo{
    private String catalog2Id; // 父分类，2级分类ID
    private String id;
    private String name;
  }

}
