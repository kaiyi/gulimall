package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.service.SkuInfoService;
import com.atguigu.gulimall.product.vo.SkuItemVo;
import com.atguigu.gulimall.product.vo.SpuItemAttrGroupVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallProductApplicationTests {
  @Autowired
  BrandService brandService;

  @Autowired
  CategoryService categoryService;

  @Autowired
  StringRedisTemplate stringRedisTemplate;

  @Autowired
  AttrGroupService attrGroupService;

  @Autowired
  SkuInfoService skuInfoService;

  @Test
  public void contextLoads() {
    BrandEntity brandEntity = new BrandEntity();

    // 插入
    // brandEntity.setName("苹果");
    // brandService.save(brandEntity);
    // System.out.println("保存成功...");

    // 更新
//    brandEntity.setDescript("我要吃苹果");
//    brandEntity.setBrandId(1L);
//    brandService.updateById(brandEntity);
//    System.out.println("更新成功...");

    // 查询
    List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id", 1L));
//    for (BrandEntity entity : list) {
//
//    }

    list.forEach((item)->{
      System.out.println(item);
    });

    // 打印结果：
    // BrandEntity(brandId=1, name=苹果, logo=null, descript=我要吃苹果, showStatus=null, firstLetter=null, sort=null)
  }

  @Test
  public void CateTreeTest(){

    List<CategoryEntity> cateChildren = categoryService.listWithTree();
    System.out.println(cateChildren);

  }


  @Test
  public void CatePath(){
    List<CategoryEntity> entities =  categoryService.getLevel1Categorys();
    System.out.println(entities);

  }

  @Test
  public void streamTest(){

    List<String> list = new ArrayList<>();
    list.add("lily");
    list.add("corwien");
    Stream<String> stream = list.stream();
    System.out.println("获取顺序流");
    System.out.println(stream);

    Stream<String> parallelStream = list.parallelStream(); //获取一个并行流
    System.out.println(parallelStream);

  }

  @Test
  public void redisTest(){
    ValueOperations<String, String> ops = stringRedisTemplate.opsForValue();
    ops.set("hello", "lily");

    String key = "hello";
    System.out.println(ops.get(key));


  }

  @Test
  public void attrGroupTest(){
    List<SpuItemAttrGroupVo> groupVos = attrGroupService.getAttrGroupWithAttrsBySpuId(5L, 225L);
    System.out.println(groupVos);

  }

  @Test
  public void itemTest() throws ExecutionException, InterruptedException {

    Long skuId = 1L;
    SkuItemVo itemVo = skuInfoService.item(skuId);

    System.out.println(itemVo);
  }

}
