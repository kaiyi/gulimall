package com.atguigu.gulimall.search;

import com.atguigu.gulimall.search.config.GulimallElasticSearchConfig;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallSearchApplicationTests {

  @Qualifier("elasticsearchRestHighLevelClient")
  @Autowired
  private RestHighLevelClient client;

  @Test
  public void contextLoads() {

    System.out.println(client);
  }

  @Test
  public void searchData() throws IndexOutOfBoundsException, IOException {
    // 1、创建检索请求
    SearchRequest searchRequest = new SearchRequest();
    // 指定索引
    searchRequest.indices("bank");
    // 指定DSL，检索条件
    // SearchSourceBuilder sourceBuilder 封装的条件
    SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

    // 1.1)、构造检索条件
//    sourceBuilder.query();
//    sourceBuilder.from();
//    sourceBuilder.size();
//    sourceBuilder.aggregation()
    sourceBuilder.query(QueryBuilders.matchQuery("address", "mill"));

    // 1.2)、按照年龄的值分布进行聚合
    TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
    sourceBuilder.aggregation(ageAgg);

    // 2、执行检索
    SearchResponse searchResponse = client.search(searchRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);

    // 3、分析结果 searchResponse
    System.out.println("检索条件"+searchResponse.toString());
    // searchRequest.source(sourceBuilder);
  }

}
