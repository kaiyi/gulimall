
# 商城业务-检索服务
1）`gulimall_product_mapping.json`
  该文件为 `gulimall_product` 的映射文件，执行时，直接在kibana中，执行PUT `gulimall_product`，即可；

2）`gulimall_product_dsl.json`
  该文件为复杂检索条件，直接在kibana中，执行GET `/gulimall_product/_search` 即可；
  
3) `gulimall_product.json`
  该文件为查询的检索结果，用于参考product数据结构；