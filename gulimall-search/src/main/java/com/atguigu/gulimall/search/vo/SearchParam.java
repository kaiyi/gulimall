package com.atguigu.gulimall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * @Description: 封装页面所有可能传递过来的查询条件
 *
 * @example:
 * keyword=小米 &sort=saleCount_desc/asc&hasStock=0/1&skuPrice=400_1900&brandId=1&catalog3Id=1&at trs=1_3G:4G:5G&attrs=2_骁龙845&attrs=4_高清屏
 * @author: kaiyi
 * @create: 2020-09-03 01:27
 */
@Data
public class SearchParam {
  private String keyword;   // 页面传递过来的全文匹配关键字
  private Long catalog3Id;  // 三级分类ID


  /**
   * 排序条件：sort=price/salecount/hotscore_desc/asc
   */
  private String sort;

  /**
   * 过滤条件：hasStock、skuPrice区间、brandId、catalog3Id、attrs
   * hasStock=0/1
   * skuPrice=1_500/_500/500_1000
   * brandId=1
   * attrs=2_5寸:6寸 （2表示属性ID，后边:表示多选）
   */
  private Integer hasStock; // 是否只显示有货
  private String skuPrice; // 价格区间查询
  private List<Long> brandId; // 按照品牌进行查询，可以多选
  private List<String> attrs; // 按照属性进行分组

  private Integer pageNum = 1; // 页码,默认赋值为1

  /**
   * 原生的所有查询条件
   */
  private String queryString;


}
