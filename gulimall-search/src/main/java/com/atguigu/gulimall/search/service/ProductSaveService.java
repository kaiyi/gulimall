package com.atguigu.gulimall.search.service;

import com.atguigu.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author: kaiyi
 * @create: 2020-08-27 16:00
 */
public interface ProductSaveService {

  Boolean prudoctStatusUp(List<SkuEsModel> skuEsModels) throws IOException;
}
