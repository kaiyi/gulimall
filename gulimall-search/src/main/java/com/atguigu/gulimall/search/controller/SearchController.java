package com.atguigu.gulimall.search.controller;

import com.atguigu.gulimall.search.service.MallSearchService;
import com.atguigu.gulimall.search.vo.SearchParam;
import com.atguigu.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author: kaiyi
 * @create: 2020-09-03 00:37
 */
@Controller
public class SearchController {

  @Autowired
  MallSearchService mallSearchService;

  /**
   * SpringMvc会自动将页面提交过来的所有请求参数封装成我们指定的对象
   * @param param
   * @return
   */
  @GetMapping("/list.html")
  public String listPage(SearchParam param, Model model){

    // 1、根据传递过来的页面查询参数，去es中检索商品
    SearchResult result = mallSearchService.search(param);
    model.addAttribute("result", result);
    return "list";
  }

}
