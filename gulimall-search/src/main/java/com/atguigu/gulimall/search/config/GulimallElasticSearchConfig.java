package com.atguigu.gulimall.search.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ES配置
 *
 * @author: kaiyi
 * @create: 2020-08-25 11:09
 *
 * 步骤：
 * 1、导入依赖
 * 2、编写配置，给容器中注入一个RestHighLevelClient
 */
@Configuration
public class GulimallElasticSearchConfig {

  // see doc https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.9/java-rest-low-usage-requests.html#java-rest-low-usage-request-options
  public static final RequestOptions COMMON_OPTIONS;
  static {
    RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
//    builder.addHeader("Authorization", "Bearer " + TOKEN);
//    builder.setHttpAsyncResponseConsumerFactory(
//        new HttpAsyncResponseConsumerFactory
//            .HeapBufferedResponseConsumerFactory(30 * 1024 * 1024 * 1024));
    COMMON_OPTIONS = builder.build();
  }

  @Bean
  public RestHighLevelClient esRestClient(){
    // see offical doc
    // https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.x/java-rest-high-getting-started-initialization.html

    RestHighLevelClient client = new RestHighLevelClient(
        RestClient.builder(
            new HttpHost("192.168.10.10", 9200, "http")));

    return client;
  }

}
