package com.atguigu.gulimall.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

//在服务启动时会自动扫描Feign包下的路径
@EnableFeignClients(basePackages = "com.atguigu.gulimall.search.feign")
@EnableDiscoveryClient // 添加注册发现功能
@SpringBootApplication
public class GulimallSearchApplication {

  public static void main(String[] args) {
    SpringApplication.run(GulimallSearchApplication.class, args);
  }

}
