package com.atguigu.gulimall.search.constant;

/**
 * @author: kaiyi
 * @create: 2020-08-27 16:38
 */
public class EsConstant {

  //在es中的索引
  public static final String PRODUCT_INDEX = "gulimall_product";
  public static final Integer PRODUCT_PAGESIZE = 10;

}
