package com.atguigu.gulimall.auth;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.auth.feign.ThirdPartFeignService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallAuthServerApplicationTests {

  @Autowired
  ThirdPartFeignService thirdPartFeignService;

  @Test
  public void contextLoads() {

    // 2.验证码的再次校验。
    // String code = UUID.randomUUID().toString().substring(0, 6);

    // System.out.println(code);
    System.out.println(Math.random() * 9 + 1);
    int code = (int) ((Math.random() * 9 + 1) * 100000);
    System.out.println(code);

  }

  @Test
  public void sendTest(){

    String phone = "15820226336";
    String codeNum = "123456";
    R r = thirdPartFeignService.sendCode(phone, codeNum);
    System.out.println(r);

  }

}
