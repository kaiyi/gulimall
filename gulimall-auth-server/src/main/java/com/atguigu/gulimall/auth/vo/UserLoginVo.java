package com.atguigu.gulimall.auth.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-09-09 00:18
 */
@Data
public class UserLoginVo {
  private String loginacct;

  private String password;

}
