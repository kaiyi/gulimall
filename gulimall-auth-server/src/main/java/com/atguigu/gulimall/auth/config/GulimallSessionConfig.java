package com.atguigu.gulimall.auth.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

/**
 * @author: kaiyi
 * @create: 2020-09-09 17:05
 */
@Configuration
public class GulimallSessionConfig {

  /**
   * 子域名共享设置及session名自定义
   * @return
   */
  @Bean
  public CookieSerializer cookieSerializer(){
    DefaultCookieSerializer defaultCookieSerializer = new DefaultCookieSerializer();

    defaultCookieSerializer.setDomainName("gulimall.com");   // 设置作用域
    defaultCookieSerializer.setCookieName("GULISESSION");     // 设置session名

    return defaultCookieSerializer;
  }

  /**
   * 默认序列化转为JSON存储
   *
   * @return
   */
  @Bean
  public RedisSerializer<Object> springSessionDefaultRedisSerializer(){
    return new GenericJackson2JsonRedisSerializer();
  }



}
