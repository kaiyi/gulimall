package com.atguigu.gulimall.auth.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: kaiyi
 * @create: 2020-09-06 15:00
 */
@Configuration
public class GulimallWebConfig implements WebMvcConfigurer {


  /**
   * 视图映射
   *
   * @param registry
   */
  @Override
  public void addViewControllers(ViewControllerRegistry registry) {

    /**
     * 等价于：
     *
     *  @GetMapping("/login.html")
     *   public String loginPage(){
     *     return "login";
     *   }
     */
    registry.addViewController("/login.html").setViewName("login");
    registry.addViewController("/reg.html").setViewName("reg");

  }
}
