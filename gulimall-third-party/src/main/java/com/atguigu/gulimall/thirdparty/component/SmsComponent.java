package com.atguigu.gulimall.thirdparty.component;

import com.atguigu.common.utils.HttpUtils;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: kaiyi
 * @create: 2020-09-06 16:14
 */
@ConfigurationProperties(prefix = "spring.cloud.alicloud.sms")
@Data
@Component
public class SmsComponent {

  private String host;
  private String path;
  private String templateId;
  private String sign;
  private Integer expireTime;  // 过期时间，几分钟后过期
  private String appcode;

  public void sendCode(String phone,String code) {
    String method = "POST";
    Map<String, String> headers = new HashMap<String, String>();
    //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
    headers.put("Authorization", "APPCODE " + appcode);
    //根据API的要求，定义相对应的Content-Type
    headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    Map<String, String> querys = new HashMap<String, String>();
    Map<String, String> bodys = new HashMap<String, String>();
    bodys.put("callbackUrl", "http://test.dev.esandcloud.com");
    bodys.put("channel", "0");
    bodys.put("mobile", phone);
    bodys.put("templateID", templateId);  // 短信模板ID，这里先写死，使用测试模板，正式模板需要自己申请。
    bodys.put("templateParamSet", "['"+code+"', '1']");

    try {
      /**
       * 重要提示如下:
       * HttpUtils请从
       * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
       * 下载
       *
       * 相应的依赖请参照
       * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
       */
      HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);

      System.out.println(response.toString()); //如不输出json, 请打开这行代码，打印调试头部状态码。
      //状态码: 200 正常；400 URL无效；401 appCode错误； 403 次数用完； 500 API网管错误

      //获取response的body
      //System.out.println(EntityUtils.toString(response.getEntity()));
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

}
