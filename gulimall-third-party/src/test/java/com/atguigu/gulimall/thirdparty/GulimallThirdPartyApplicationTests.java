package com.atguigu.gulimall.thirdparty;

import com.aliyun.oss.OSS;
import com.atguigu.common.utils.HttpUtils;
import com.atguigu.gulimall.thirdparty.component.SmsComponent;
import org.apache.http.HttpResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallThirdPartyApplicationTests {

  @Autowired
  public OSS ossClient;

  @Resource
  private SmsComponent smsComponent;

  @Test
  public void contextLoads() {
  }

  @Test
  public void ossTest(){
    try {
      String bucket_name = "gulimall-corwien";
     FileInputStream inputStream =  new FileInputStream("/Users/kaiyiwang/Desktop/toutiao/unnamed.jpg");
      ossClient.putObject(bucket_name, "oss-upload-test.jpg", inputStream);
    }
    catch (Exception e) {
      e.printStackTrace();
      System.out.println("upload fail: " + e.getMessage());
    }

    System.out.println("upload success ");

  }

  @Test
  public void smsTest(){

    String phone = "15820226336";
    String code = "LILYO";

    //发送验证码
    smsComponent.sendCode(phone,code);

  }

  @Test
  public void smsYunTest(){
    String host = "https://edisim.market.alicloudapi.com";
    String path = "/comms/sms/sendmsg";
    String method = "POST";
    String appcode = "你自己的AppCode";
    Map<String, String> headers = new HashMap<String, String>();
    //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
    headers.put("Authorization", "APPCODE " + appcode);
    //根据API的要求，定义相对应的Content-Type
    headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    Map<String, String> querys = new HashMap<String, String>();
    Map<String, String> bodys = new HashMap<String, String>();
    bodys.put("callbackUrl", "http://test.dev.esandcloud.com");
    bodys.put("channel", "0");
    bodys.put("mobile", "15820226336");
    bodys.put("templateID", "0000000");
    bodys.put("templateParamSet", "['1234', '1']");


    try {
      /**
       * 重要提示如下:
       * HttpUtils请从
       * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
       * 下载
       *
       * 相应的依赖请参照
       * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
       */
      HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
      System.out.println(response.toString());
      //获取response的body
      //System.out.println(EntityUtils.toString(response.getEntity()));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
