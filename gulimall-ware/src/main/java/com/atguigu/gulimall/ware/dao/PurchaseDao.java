package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author kaiyi
 * @email corwienwong@gmail.com
 * @date 2020-08-11 01:45:30
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
