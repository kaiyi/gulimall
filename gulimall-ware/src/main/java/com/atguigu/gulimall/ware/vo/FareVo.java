package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 运费vo
 * @author: kaiyi
 * @create: 2020-09-15 00:05
 */
@Data
public class FareVo {

  private MemberAddressVo address;

  private BigDecimal fare;

}
