package com.atguigu.gulimall.ware.service;

import com.atguigu.gulimall.ware.vo.FareVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.WareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author kaiyi
 * @email corwienwong@gmail.com
 * @date 2020-08-11 01:45:30
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

  /**
   * 获取运费和收货地址信息
   * @param addrId
   * @return
   */
  FareVo getFare(Long addrId);
}

