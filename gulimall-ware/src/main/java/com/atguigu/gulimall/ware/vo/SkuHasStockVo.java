package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-08-27 11:57
 */
@Data
public class SkuHasStockVo {
  private Long skuId;
  private Boolean hasStock;

}
