package com.atguigu.gulimall.order.feign;

import com.atguigu.gulimall.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * 购物车远程调用
 *
 * @author: kaiyi
 * @create: 2020-09-14 11:20
 */
@FeignClient("gulimall-cart")
public interface CartFeignService {

  @GetMapping(value = "/currentUserCartItems")
  List<OrderItemVo> getCurrentCartItems();

}
