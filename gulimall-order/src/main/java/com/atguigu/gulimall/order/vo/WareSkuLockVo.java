package com.atguigu.gulimall.order.vo;

import lombok.Data;

import java.util.List;

/**
 * @author: kaiyi
 * @create: 2020-09-14 01:17
 */
@Data
public class WareSkuLockVo {

    private String orderSn;

    /** 需要锁住的所有库存信息 **/
    private List<OrderItemVo> locks;

}
