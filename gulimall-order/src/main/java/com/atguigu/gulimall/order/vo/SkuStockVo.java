package com.atguigu.gulimall.order.vo;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-09-14 01:17
 */
@Data
public class SkuStockVo {


    private Long skuId;

    private Boolean hasStock;

}
