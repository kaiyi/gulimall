package com.atguigu.gulimall.order.config;

/*
import com.zaxxer.hikari.HikariDataSource;
import io.seata.rm.datasource.DataSourceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;

 */

/**
 * Seata数据源代理配置
 *
 * @doc:
 * https://github.com/seata/seata-samples/tree/master/springcloud-jpa-seata
 *
 * @author: kaiyi
 * @create: 2020-09-15 17:44
 */
// 使用消息队列解决分布式事务一致性问题，Seata暂时不用注释掉。
// @Configuration
public class MySeataConfig {
  /*

  @Autowired
  DataSourceProperties dataSourceProperties;

   */

  /**
   * 需要将 DataSourceProxy 设置为主数据源，否则事务无法回滚
   *
   * @param dataSourceProperties
   * @return
   */
  /*
  @Bean
  public DataSource dataSource(DataSourceProperties dataSourceProperties) {

    HikariDataSource dataSource = dataSourceProperties.initializeDataSourceBuilder().type(HikariDataSource.class).build();
    if (StringUtils.hasText(dataSourceProperties.getName())) {
      dataSource.setPoolName(dataSourceProperties.getName());
    }

    return new DataSourceProxy(dataSource);
  }

   */

}
