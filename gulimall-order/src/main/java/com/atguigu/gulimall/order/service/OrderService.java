package com.atguigu.gulimall.order.service;

import com.atguigu.gulimall.order.vo.OrderConfirmVo;
import com.atguigu.gulimall.order.vo.OrderSubmitVo;
import com.atguigu.gulimall.order.vo.SubmitOrderResponseVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.order.entity.OrderEntity;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author kaiyi
 * @email corwienwong@gmail.com
 * @date 2020-08-11 01:39:17
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

  /**
   * 订单确认页返回需要用的数据
   * @return
   */
  OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;

  /**
   * 创建订单
   * @param vo
   * @return
   */
  SubmitOrderResponseVo submitOrder(OrderSubmitVo vo);

  /**
   * 查询订单状态
   * @param orderSn
   * @return
   */
  OrderEntity getOrderByOrderSn(String orderSn);

  /**
   * 关闭订单
   * @param orderEntity
   */
  void closeOrder(OrderEntity orderEntity);
}

