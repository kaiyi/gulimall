package com.atguigu.gulimall.cart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: kaiyi
 * @create: 2020-09-05 19:07
 */
@ConfigurationProperties(prefix = "gulimall.thread")
@Component
@Data
public class ThreadPoolConfigProperties {

  private Integer coreSize;
  private Integer maxSize;
  private Integer keepAliveTime;

}
