package com.atguigu.gulimall.cart.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器Web配置
 *
 * @Description:
 * 创建的拦截器必须添加到Web配置中
 *
 * @author: kaiyi
 * @create: 2020-09-12 11:14
 */

@Configuration
public class GulimallWebConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new CartInterceptor())//注册拦截器
                .addPathPatterns("/**");   // 拦截所有请求路径
    }
}
