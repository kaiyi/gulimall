package com.atguigu.gulimall.cart.to;

import lombok.Data;

/**
 * @author: kaiyi
 * @create: 2020-09-12 11:34
 */

@Data
public class UserInfoTo {

  private Long userId;

  private String userKey;

  /**
   * 是否临时用户
   */
  private Boolean tempUser = false;

}
